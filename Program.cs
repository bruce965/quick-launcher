﻿using QuickLauncher.Hotkey;
using QuickLauncher.Launcher;
using System;
using System.Windows.Forms;

namespace QuickLauncher
{
	internal sealed class Program
	{
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			
			var cmds = LauncherList.Load();
			bool firstStart = cmds == null;
			if (firstStart)
				cmds = new LauncherList();

			var initialHotkey = (HotkeyHandler)cmds.Hotkey.Clone();
			
			if (cmds.Count < 1) {
				cmds.Add(new LauncherButton() {
					X = 0, Y = 0,
					Bottom = true, Right = true,
					Label = "Customize",
					CommandType = 2	// Edit
				});
				
				cmds.Add(new LauncherButton() {
					X = 0, Y = 1,
					Bottom = true, Right = true,
					Label = "Hotkey",
					CommandType = 3	// Change Hotkey
				});
				
				cmds.Add(new LauncherButton() {
					X = 0, Y = 2,
					Bottom = true, Right = true,
					Label = "Manual",
					CommandType = 4	// Show guide
				});
			}
			
			if (firstStart || args.Length > 0 && args[0] == "-edit")
				new Editor.FormLauncher(cmds).Show();
			else
				new Graphics.FormLauncher(cmds).Show();
			
			Application.Run();
			
			if (!cmds.Hotkey.Equals(initialHotkey)) {
				var result = MessageBox.Show("Hotkey changed, do you want to save?", "Save modifications?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
				
				if (result == DialogResult.Yes)
					LauncherList.Save(cmds);
			}
		}
	}
}
