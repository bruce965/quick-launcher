﻿using System;
using System.Drawing;

namespace QuickLauncher.Launcher
{
	[Serializable]
	public class LauncherCommand : IEquatable<LauncherCommand>
	{
		public Image Icon;
		public string Label;
		public string Command;
		public string Arguments;
		public int CommandType;
		
		public bool Equals(LauncherCommand other) {
			return this.Command == other.Command && this.Arguments == other.Arguments && this.CommandType == other.CommandType;
		}
	}
}
