﻿using System;

namespace QuickLauncher.Launcher
{
	[Serializable]
	public class LauncherButton : LauncherCommand, IEquatable<LauncherButton>
	{
		public bool Bottom;
		public bool Right;
		
		public int X;
		public int Y;
		
		
		public bool Equals(LauncherButton other) {
			return this.Bottom == other.Bottom && this.Right == other.Right && this.X == other.X && this.Y == other.Y && base.Equals(other);
		}
	}
}
