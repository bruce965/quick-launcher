﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using QuickLauncher.Graphics;
using QuickLauncher.Guide;
using QuickLauncher.Hotkey;

namespace QuickLauncher.Launcher
{
	public class SpecialCommand
	{
		public static readonly SpecialCommand[] Commands = new SpecialCommand[] {
			new SpecialCommand(0, true , true , "Execute"				, (form, command) => execute(command.Command, command.Arguments)),
			new SpecialCommand(1, false, false, "Hide launcher"			, (form, command) => {}),
			new SpecialCommand(2, false, false, "Edit mode"				, (form, command) => editMode(form)),
			new SpecialCommand(3, false, false, "Change hotkey"			, (form, command) => new HotkeyForm(() => { form.UnregisterHotkey(); form.RegisterHotkey(form.Launchers.Hotkey); }, form.Launchers.Hotkey).Show()),
			new SpecialCommand(4, false, false, "Show QuickStart Guide"	, (form, command) => new GuideForm().Show())
		};
		
		public static SpecialCommand GetCommand(int id) {
			foreach(SpecialCommand cmd in Commands)
				if(cmd.ID == id)
					return cmd;
			
			return null;
		}
		
		private static void execute(string cmd, string args = null) {
			try {
				if(args == null)
					System.Diagnostics.Process.Start(cmd);
				else
					System.Diagnostics.Process.Start(cmd, args);
			} catch(Exception e) {
				Console.WriteLine(e);
			}
		}
		
		private static void editMode(FormLauncher form) {
			form.FadeOut(() => {
			    var launchers = form.Launchers;
			    form.UnregisterHotkey();
			    new Editor.FormLauncher(launchers).Show();
				form.Close();
			});
		}
		
		
		
		public readonly int ID;
		public readonly string Name;
		public readonly Action<FormLauncher, LauncherCommand> Action;
		public readonly bool AllowsCommand, AllowsArgs;
		
		public SpecialCommand(int id, bool command, bool args, string name, Action<FormLauncher, LauncherCommand> action) {
			this.ID = id;
			this.Name = name;
			this.Action = action;
			this.AllowsCommand = command;
			this.AllowsArgs = args;
		}
		
		public override string ToString() {
			return this.Name;
		}
	}
}
