﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

using QuickLauncher.Hotkey;

namespace QuickLauncher.Launcher
{
	[Serializable]
	public class LauncherList : List<LauncherButton>, IEquatable<LauncherList>
	{
		public const string DEFAULT_SAVE_PATH = "launcher.cfg";
		
		public HotkeyHandler Hotkey;
		
		public LauncherList() {
			this.Hotkey = new HotkeyHandler(true, true, false, false, Keys.R);
		}
		
		public static void Save(LauncherList list, string file = DEFAULT_SAVE_PATH) {
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None);
			formatter.Serialize(stream, list);
			stream.Close();
		}
		
		public static LauncherList Load(string file = DEFAULT_SAVE_PATH) {
			try {
				IFormatter formatter = new BinaryFormatter();
				Stream stream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
				var list = (LauncherList) formatter.Deserialize(stream);
				stream.Close();
				return list;
			} catch(FileNotFoundException) {
				return null;
			}
		}
		
		public bool Equals(LauncherList other) {
			if(this.Count != other.Count)
				return false;
			
			for(int i=0; i<this.Count; i++)
				if(!this[i].Equals(other[i]))
					return false;
			
			return this.Hotkey.Equals(other.Hotkey);
		}
	}
}
