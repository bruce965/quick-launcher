﻿using System;
using System.Drawing;
using System.Windows.Forms;

using QuickLauncher.Launcher;

namespace QuickLauncher.Hotkey
{
	public partial class HotkeyForm : Form
	{
		private readonly Action callback;
		
		public readonly HotkeyHandler Hotkey;
		
		public HotkeyForm(Action callback, HotkeyHandler hotkey) {
			this.KeyPreview = true;
			InitializeComponent();
			
			this.callback = callback;
			
			this.winkey.Checked = hotkey.Win;
			this.ctrl.Checked = hotkey.Ctrl;
			this.shift.Checked = hotkey.Shift;
			this.alt.Checked = hotkey.Alt;
			this.Hotkey = hotkey;
			
			this.key.Text = hotkey.Key.ToString();
		}
		
		private void KeyClick(object sender, EventArgs e) {
			this.key.Enabled = false;
		}
		
		private void HotkeyFormKeyDown(object sender, KeyEventArgs e) {
			if(!this.key.Enabled) {
				this.key.Enabled = true;
				
				this.key.Text = e.KeyCode.ToString();
				this.Hotkey.Key = e.KeyCode;
			}
		}
		
		private void OkClick(object sender, EventArgs e) {
			callback();
			this.Close();
		}
		
		private void keyCheckedChanged(object sender, EventArgs e) {
			if(this.Hotkey == null)
				return;
			
			this.Hotkey.Win = this.winkey.Checked;
			this.Hotkey.Ctrl = this.ctrl.Checked;
			this.Hotkey.Shift = this.shift.Checked;
			this.Hotkey.Alt = this.alt.Checked;
		}
		
		private void HotkeyFormFormClosing(object sender, FormClosingEventArgs e) {
			callback();
		}
	}
}
