﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace QuickLauncher.Hotkey
{
	[Serializable]
	public sealed class HotkeyHandler : IEquatable<HotkeyHandler>, ICloneable
	{
        private const int ALT = 0x0001;
        private const int CTRL = 0x0002;
        private const int SHIFT = 0x0004;
        private const int WIN = 0x0008;

        //windows message id for hotkey
        public const int WM_HOTKEY_MSG_ID = 0x0312;
		
		[DllImport("user32.dll")]
		private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);
		
		[DllImport("user32.dll")]
		private static extern bool UnregisterHotKey(IntPtr hWnd, int id);
		
		[NonSerialized]
		private IntPtr hWnd;
		
		[NonSerialized]
		private int id;
		public bool IsRegistered { get { return id != 0; } }
		
		private int modifier;
		public bool Alt {
			get { return (modifier & ALT) != 0; }
			set { if(value) modifier |= ALT; else modifier &= ~ALT; }
		}
		public bool Ctrl {
			get { return (modifier & CTRL) != 0; }
			set { if(value) modifier |= CTRL; else modifier &= ~CTRL; }
		}
		public bool Shift {
			get { return (modifier & SHIFT) != 0; }
			set { if(value) modifier |= SHIFT; else modifier &= ~SHIFT; }
		}
		public bool Win {
			get { return (modifier & WIN) != 0; }
			set { if(value) modifier |= WIN; else modifier &= ~WIN; }
		}
		
		public Keys Key { get; set; }
		
		public bool Register(IntPtr hWnd) {
			if(this.IsRegistered) {
				if(this.hWnd == hWnd)
					return true;
				
				this.Unregister();
			}
			
			this.hWnd = hWnd;
			
			id = this.GetHashCode();
			var reg = RegisterHotKey(hWnd, id, modifier, (int) Key);
			
			if(!reg)
				id = 0;
			
		    return reg;
		}
		
		public bool Register(Form form) {
			return Register(form.Handle);
		}
		
		public bool Unregister() {
			var unreg = UnregisterHotKey(hWnd, id);
			id = 0;
		    return unreg;
		}
		
		public HotkeyHandler() {}
		
		public HotkeyHandler(bool alt, bool ctrl, bool shift, bool win, Keys key) : this() {
			this.Alt = alt;
			this.Ctrl = ctrl;
			this.Shift = shift;
			this.Win = win;
			this.Key = key;
		}
		
		public override string ToString() {
			return string.Format("{0}{1}{2}{3}{4}",
				this.Win   ? "WIN + "   : "",
				this.Ctrl  ? "CTRL + "  : "",
				this.Shift ? "SHIFT + " : "",
				this.Alt   ? "ALT + "   : "",
				((Keys) Key).ToString()
			);
		}
		
		public override int GetHashCode() {
			return modifier ^ ((int) Key) ^ hWnd.ToInt32();
		}
		
		public bool Equals(HotkeyHandler other) {
			if(other == null)
				return false;
			
			return this.modifier == other.modifier && this.Key == other.Key;
		}
		
		public object Clone() {
			return new HotkeyHandler(this.Alt, this.Ctrl, this.Shift, this.Win, this.Key);
		}
	}
}
