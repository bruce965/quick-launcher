﻿/*
 * Created by SharpDevelop.
 * User: Fabio Iotti
 * Date: 10/04/2014
 * Time: 11.22
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace QuickLauncher.Hotkey
{
	partial class HotkeyForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.ctrl = new System.Windows.Forms.CheckBox();
			this.alt = new System.Windows.Forms.CheckBox();
			this.winkey = new System.Windows.Forms.CheckBox();
			this.shift = new System.Windows.Forms.CheckBox();
			this.key = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.ok = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// ctrl
			// 
			this.ctrl.Location = new System.Drawing.Point(67, 28);
			this.ctrl.Name = "ctrl";
			this.ctrl.Size = new System.Drawing.Size(55, 24);
			this.ctrl.TabIndex = 0;
			this.ctrl.Text = "CTRL";
			this.ctrl.UseVisualStyleBackColor = true;
			this.ctrl.CheckedChanged += new System.EventHandler(this.keyCheckedChanged);
			// 
			// alt
			// 
			this.alt.Location = new System.Drawing.Point(193, 28);
			this.alt.Name = "alt";
			this.alt.Size = new System.Drawing.Size(48, 24);
			this.alt.TabIndex = 1;
			this.alt.Text = "ALT";
			this.alt.UseVisualStyleBackColor = true;
			this.alt.CheckedChanged += new System.EventHandler(this.keyCheckedChanged);
			// 
			// winkey
			// 
			this.winkey.Location = new System.Drawing.Point(12, 28);
			this.winkey.Name = "winkey";
			this.winkey.Size = new System.Drawing.Size(49, 24);
			this.winkey.TabIndex = 2;
			this.winkey.Text = "WIN";
			this.winkey.UseVisualStyleBackColor = true;
			this.winkey.CheckedChanged += new System.EventHandler(this.keyCheckedChanged);
			// 
			// shift
			// 
			this.shift.Location = new System.Drawing.Point(128, 28);
			this.shift.Name = "shift";
			this.shift.Size = new System.Drawing.Size(59, 24);
			this.shift.TabIndex = 3;
			this.shift.Text = "SHIFT";
			this.shift.UseVisualStyleBackColor = true;
			this.shift.CheckedChanged += new System.EventHandler(this.keyCheckedChanged);
			// 
			// key
			// 
			this.key.Location = new System.Drawing.Point(247, 21);
			this.key.Name = "key";
			this.key.Size = new System.Drawing.Size(123, 36);
			this.key.TabIndex = 4;
			this.key.UseVisualStyleBackColor = true;
			this.key.Click += new System.EventHandler(this.KeyClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(229, 16);
			this.label1.TabIndex = 5;
			this.label1.Text = "Current hotkey:";
			// 
			// ok
			// 
			this.ok.Location = new System.Drawing.Point(295, 63);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(75, 23);
			this.ok.TabIndex = 9;
			this.ok.Text = "OK";
			this.ok.UseVisualStyleBackColor = true;
			this.ok.Click += new System.EventHandler(this.OkClick);
			// 
			// HotkeyForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(382, 98);
			this.Controls.Add(this.ok);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.key);
			this.Controls.Add(this.shift);
			this.Controls.Add(this.winkey);
			this.Controls.Add(this.alt);
			this.Controls.Add(this.ctrl);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "HotkeyForm";
			this.Text = "Change Hotkey";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HotkeyFormFormClosing);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HotkeyFormKeyDown);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button key;
		private System.Windows.Forms.CheckBox shift;
		private System.Windows.Forms.CheckBox winkey;
		private System.Windows.Forms.CheckBox alt;
		private System.Windows.Forms.CheckBox ctrl;
	}
}
