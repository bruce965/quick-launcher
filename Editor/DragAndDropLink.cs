﻿using System;

namespace QuickLauncher.Editor
{
	public class DragAndDropLink<T>
	{
		public readonly T Value;
		
		public DragAndDropLink(T value) {
			this.Value = value;
		}
	}
}
