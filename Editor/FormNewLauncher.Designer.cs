﻿/*
 * Created by SharpDevelop.
 * User: Fabio Iotti
 * Date: 03/04/2014
 * Time: 09.13
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace QuickLauncher.Editor
{
	partial class FormNewLauncher
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNewLauncher));
			this.icon = new System.Windows.Forms.PictureBox();
			this.label = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.command = new System.Windows.Forms.TextBox();
			this.ok = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.remove = new System.Windows.Forms.Button();
			this.arguments = new System.Windows.Forms.TextBox();
			this.action = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.icon)).BeginInit();
			this.SuspendLayout();
			// 
			// icon
			// 
			this.icon.Image = ((System.Drawing.Image)(resources.GetObject("icon.Image")));
			this.icon.Location = new System.Drawing.Point(12, 12);
			this.icon.Name = "icon";
			this.icon.Size = new System.Drawing.Size(32, 32);
			this.icon.TabIndex = 0;
			this.icon.TabStop = false;
			// 
			// label
			// 
			this.label.Location = new System.Drawing.Point(12, 72);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(427, 20);
			this.label.TabIndex = 1;
			this.label.Text = "Launch";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(50, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(389, 35);
			this.label1.TabIndex = 2;
			this.label1.Text = "Drag here an icon for the button";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 53);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "Button label";
			this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(12, 95);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(248, 28);
			this.label3.TabIndex = 6;
			this.label3.Text = "Button command + arguments";
			this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// command
			// 
			this.command.Location = new System.Drawing.Point(12, 126);
			this.command.Name = "command";
			this.command.Size = new System.Drawing.Size(300, 20);
			this.command.TabIndex = 4;
			this.command.Text = "notepad.exe";
			// 
			// ok
			// 
			this.ok.Location = new System.Drawing.Point(364, 200);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(75, 23);
			this.ok.TabIndex = 8;
			this.ok.Text = "OK";
			this.ok.UseVisualStyleBackColor = true;
			this.ok.Click += new System.EventHandler(this.OkClick);
			// 
			// cancel
			// 
			this.cancel.Location = new System.Drawing.Point(283, 200);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 9;
			this.cancel.Text = "Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			this.cancel.Click += new System.EventHandler(this.CancelClick);
			// 
			// remove
			// 
			this.remove.Location = new System.Drawing.Point(202, 200);
			this.remove.Name = "remove";
			this.remove.Size = new System.Drawing.Size(75, 23);
			this.remove.TabIndex = 10;
			this.remove.Text = "Remove";
			this.remove.UseVisualStyleBackColor = true;
			this.remove.Visible = false;
			this.remove.Click += new System.EventHandler(this.RemoveClick);
			// 
			// arguments
			// 
			this.arguments.Location = new System.Drawing.Point(12, 152);
			this.arguments.Name = "arguments";
			this.arguments.Size = new System.Drawing.Size(427, 20);
			this.arguments.TabIndex = 11;
			this.arguments.Text = "example.txt";
			// 
			// action
			// 
			this.action.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.action.FormattingEnabled = true;
			this.action.Location = new System.Drawing.Point(318, 126);
			this.action.Name = "action";
			this.action.Size = new System.Drawing.Size(121, 21);
			this.action.TabIndex = 12;
			this.action.SelectedIndexChanged += new System.EventHandler(this.ActionSelectedIndexChanged);
			// 
			// FormNewLauncher
			// 
			this.AllowDrop = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(451, 235);
			this.Controls.Add(this.action);
			this.Controls.Add(this.arguments);
			this.Controls.Add(this.remove);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.ok);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.command);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label);
			this.Controls.Add(this.icon);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FormNewLauncher";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Add Launcher";
			((System.ComponentModel.ISupportInitialize)(this.icon)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.ComboBox action;
		private System.Windows.Forms.TextBox arguments;
		private System.Windows.Forms.Button remove;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.TextBox command;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox label;
		private System.Windows.Forms.PictureBox icon;
	}
}
