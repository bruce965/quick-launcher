﻿using QuickLauncher.Launcher;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace QuickLauncher.Editor
{
	public class ButtonLauncher : Graphics.ButtonLauncher
	{
		private static readonly ComponentResourceManager resources = new ComponentResourceManager(typeof(ButtonLauncher));
		private static readonly Image plusImage = (Image) resources.GetObject("list-add");
		
		private readonly FormLauncher parent;
		
		public LauncherCommand Command { get; private set; }
		
		public readonly bool RightAnchor, BottomAnchor;
		public readonly int X, Y;
		
		public ButtonLauncher(FormLauncher parent, bool right, bool bottom, int x, int y, LauncherCommand cmd) : this(parent, right, bottom, x, y) {
			UpdateCommand(cmd);
		}
		
		public ButtonLauncher(FormLauncher parent, bool right, bool bottom, int x, int y) : base(parent, right, bottom, x, y, null) {
			this.Image = plusImage;
			
			this.AllowDrop = true;
			
			this.parent = parent;
			
			this.RightAnchor = right;
			this.BottomAnchor = bottom;
			this.X = x;
			this.Y = y;
		}
		
		protected override void OnClick(EventArgs e) {
			parent.FadeOut();
			new FormNewLauncher(cmd => {
				if(cmd != null)
					UpdateCommand(cmd == FormNewLauncher.RemoveCommand ? null : cmd);
				
				parent.FadeIn();
			}, Command).Show();
			
			base.OnClick(e);
		}
		
		public void UpdateCommand(LauncherCommand cmd) {
			this.Command = cmd;
			
			if(cmd != null) {
				this.Image = cmd.Icon;
				this.Text = cmd.Label;
				
				this.ImageAlign = ContentAlignment.MiddleLeft;
			} else {
				this.Image = plusImage;
				this.Text = string.Empty;
				
				this.ImageAlign = ContentAlignment.MiddleCenter;
			}
			
			this.parent.Modified = true;
		}
		
		
		
		private static Point? mouseDown = null;
		
		protected override void OnMouseDown(MouseEventArgs mevent) {
			mouseDown = mevent.Location;
			base.OnMouseDown(mevent);
		}
		
		protected override void OnMouseUp(MouseEventArgs mevent) {
			mouseDown = null;
			base.OnMouseUp(mevent);
		}
		
		protected override void OnMouseMove(MouseEventArgs mevent) {
			if(mouseDown != null && this.Command != null && IsDragGesture(mevent.Location, mouseDown.Value))
				parent.DoDragDrop(new DragAndDropLink<LauncherCommand>(this.Command), DragDropEffects.All);
			
			base.OnMouseMove(mevent);
		}
		
		protected override void OnDragDrop(DragEventArgs drgevent) {
			DragAndDropLink<LauncherCommand> data = (DragAndDropLink<LauncherCommand>) drgevent.Data.GetData(typeof(DragAndDropLink<LauncherCommand>));
			this.UpdateCommand(data.Value);
			
			mouseDown = null;
			
			base.OnDragDrop(drgevent);
		}
		
		protected override void OnDragEnter(DragEventArgs drgevent) {
			if(drgevent.Data.GetDataPresent(typeof(DragAndDropLink<LauncherCommand>)))
				drgevent.Effect = DragDropEffects.Copy;
			
			base.OnDragEnter(drgevent);
		}
		
		private static bool IsDragGesture(Point point, Point start) {
			var horizontalMove = Math.Abs(point.X - start.X);  
			var verticalMove = Math.Abs(point.Y - start.Y);  
			var hGesture = horizontalMove > 20;  
			var vGesture = verticalMove > 20;  
			return hGesture | vGesture;  
		}
	}
}
