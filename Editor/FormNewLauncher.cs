﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using QuickLauncher.Launcher;

namespace QuickLauncher.Editor
{
	public partial class FormNewLauncher : Form
	{
		public static readonly LauncherCommand RemoveCommand = new LauncherCommand();
		
		private readonly Action<LauncherCommand> callback;
		
		protected FormNewLauncher() {
			InitializeComponent();
			
			this.icon.SizeMode = PictureBoxSizeMode.CenterImage;
			
			this.action.Items.AddRange(SpecialCommand.Commands);
			this.action.SelectedIndex = 0;
		}
		
		public FormNewLauncher(Action<LauncherCommand> callback, LauncherCommand cmd) : this() {
			if(cmd != null) {
				this.Text = "Edit Launcher";
				this.remove.Visible = true;
				
				this.icon.Image = cmd.Icon;
				this.label.Text = cmd.Label;
				this.command.Text = cmd.Command;
				this.arguments.Text = cmd.Arguments;
				this.action.SelectedIndex = cmd.CommandType;
			}
			
			this.callback = callback;
		}
		
		protected override void OnDragDrop(DragEventArgs drgevent) {
			try {
				string[] files = (string[])drgevent.Data.GetData(DataFormats.FileDrop);
				icon.Image = Image.FromFile(files[0]);
			} catch(Exception) {
				try {
					string[] files = (string[])drgevent.Data.GetData(DataFormats.FileDrop);
					this.label.Text = Path.GetFileNameWithoutExtension(files[0]);
					this.command.Text = files[0];
					this.arguments.Text = string.Empty;
					this.icon.Image = Icon.ExtractAssociatedIcon(files[0]).ToBitmap();
				} catch(Exception) { }
			}
			
			base.OnDragDrop(drgevent);
		}
		
		protected override void OnDragEnter(DragEventArgs drgevent) {
			drgevent.Effect = DragDropEffects.Move;
		}
		
		protected override void OnClosing(CancelEventArgs e) {
			callback(null);
		}
		
		private void OkClick(object sender, EventArgs e) {
			callback(new LauncherCommand() {
				Icon = this.icon.Image,
				Label = this.label.Text,
				Command = this.command.Text,
				Arguments = this.arguments.Text,
				CommandType = this.action.SelectedIndex
			});
			
			this.Close();
		}
		
		private void CancelClick(object sender, EventArgs e) {
			callback(null);
			this.Close();
		}
		
		private void RemoveClick(object sender, EventArgs e) {
			callback(RemoveCommand);
			this.Close();
		}
		
		private void ActionSelectedIndexChanged(object sender, EventArgs e) {
			var cmd = SpecialCommand.GetCommand(this.action.SelectedIndex);
			
			this.command.Enabled = cmd.AllowsCommand;
			this.arguments.Enabled = cmd.AllowsArgs;
		}
	}
}