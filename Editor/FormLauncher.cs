﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using QuickLauncher.Launcher;

namespace QuickLauncher.Editor
{
	public class FormLauncher : Graphics.FormLauncher
	{
		public bool Modified;
		
		public FormLauncher(LauncherList buttons = null) {
			for(int x=0; x<5; x++) {
				for(int y=0; y<5; y++) {
					addButton(false, false, x, y, buttons);
					addButton(false, true , x, y, buttons);
					addButton(true , false, x, y, buttons);
					addButton(true , true , x, y, buttons);
				}
			}
			
			int i = 0;
			foreach(Control control in this.Controls.Cast<Control>().OrderBy(c => c.Location.Y).ThenBy(c => c.Location.X))
				control.TabIndex = i++;
			
			this.Launchers = buttons;
			
			this.Modified = false;
			this.FadeIn();
		}
		
		
		
		protected override void OnClosing(CancelEventArgs e) {
			if(Modified) {
				// TODO: currently also saves hotkey if present, should ask/prompt to user.
				// TODO: if hotkey is already saved, prevent from asking again when closing program.
				var result = MessageBox.Show("Launchers layout changed, do you want to save?", "Save modifications?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3);
				
				if(result == DialogResult.Cancel) {
					this.FadeIn();
					e.Cancel = true;
					return;
				}
				
				if(result == DialogResult.Yes) {
					var launchers = getLauncherList();
					
					this.Launchers.Clear();
					this.Launchers.InsertRange(0, launchers);
					
					LauncherList.Save(this.Launchers);
				}
			}
			
			base.OnClosing(e);
		}
		
		protected override void OnFormClosing(FormClosingEventArgs e) {
			this.UnregisterHotkey();
			new Graphics.FormLauncher(this.Launchers).FadeIn();
			base.OnFormClosing(e);
		}
		
		
		
		private void addButton(bool right, bool bottom, int x, int y, LauncherList buttons) {
			var button = new ButtonLauncher(this, right, bottom, x, y);
			
			if(buttons != null)
				foreach(var btn in buttons)
					if(btn.X == x && btn.Y == y && btn.Right == right && btn.Bottom == bottom)
						button.UpdateCommand(btn);
			
			this.Controls.Add(button);
		}
		
		private List<LauncherButton> getLauncherList() {
			List<LauncherButton> list = new LauncherList();
			
			foreach(ButtonLauncher button in this.Controls.OfType<ButtonLauncher>()) {
				if(button.Command != null) {
					list.Add(new LauncherButton() {
						Right = button.RightAnchor,
						Bottom = button.BottomAnchor,
						X = button.X,
						Y = button.Y,
						Icon = button.Command.Icon,
						Label = button.Command.Label,
						Arguments = button.Command.Arguments,
						Command = button.Command.Command,
						CommandType = button.Command.CommandType
					});
				}
			}
			
			return list;
		}
	}
}
