﻿using QuickLauncher.Launcher;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace QuickLauncher.Graphics
{
	public class ButtonLauncher : Button
	{
		private const int SPACING = 10;
		
		private readonly LauncherCommand command;
		private readonly FormLauncher parent;
		
		public ButtonLauncher(FormLauncher parent, bool right, bool bottom, int x, int y, LauncherCommand cmd) {
			this.parent = parent;

			Size = new Size(100, 40);
			FlatStyle = FlatStyle.Flat;
			UseVisualStyleBackColor = false;
			FlatAppearance.BorderSize = 0;
			Cursor = Cursors.Hand;
			TextAlign = ContentAlignment.MiddleRight;

			if (cmd != null) {
				Text = cmd.Label;
				Image = cmd.Icon;

				ImageAlign = ContentAlignment.MiddleLeft;

				command = cmd;
			}

			Anchor = (right ? AnchorStyles.Right : AnchorStyles.Left) | (bottom ? AnchorStyles.Bottom : AnchorStyles.Top);

			Location = new Point(right ? parent.ClientSize.Width- Width - SPACING : SPACING, bottom ? parent.ClientSize.Height- Height - SPACING : SPACING);
			Location = new Point(Location.X + (right ? -1 : +1) * (Width + SPACING) * x, Location.Y + (bottom ? -1 : +1) * (Height + SPACING) * y);

			RefreshColor();
		}

		public void RefreshColor() {
			Color buttonColor = parent.BackColor;
			if (parent.BackgroundImage == null)
				buttonColor = buttonColor.GetBrightness() < 0.5 ? Color.White : Color.Black;

			ForeColor = buttonColor.GetBrightness() < 0.5 ? Color.White : Color.Black;
			BackColor = buttonColor;
		}
		
		protected override void OnClick(EventArgs e) {
			if (command != null) {
				parent.FadeOut();
				SpecialCommand.GetCommand(command.CommandType).Action(parent, command);
			}
			
			base.OnClick(e);
		}
	}
}
