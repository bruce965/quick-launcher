﻿using QuickLauncher.Hotkey;
using QuickLauncher.Launcher;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace QuickLauncher.Graphics
{
	public class FormLauncher : Form
	{
		public LauncherList Launchers;
		
		private static int instances = 0;

		private string lastBackgroundImagePath;
		private Color lastBackgroundColor;

		public FormLauncher(LauncherList buttons) : this() {
			if(buttons != null)
				Launchers = buttons;
			
			foreach(var button in Launchers)
				Controls.Add(new ButtonLauncher(this, button.Right, button.Bottom, button.X, button.Y, button));
		}
		
		public FormLauncher() {
			instances++;
			
			Size = new Size(1024, 768);
			FormBorderStyle = FormBorderStyle.None;
			ShowInTaskbar = false;
			WindowState = FormWindowState.Maximized;
			DoubleBuffered = true;
			Opacity = 0;
			
			#if !DEBUG
			// frozen topmost windows are annoying while debugging
			TopMost = true;
			#endif
			
			RefreshBackground();
			
			Launchers = new LauncherList();
		}
		
		public void FadeIn() {
			if (Opacity != 0f)
				return;

			RefreshBackground();
			Show();
			Refresh();
			BringToFront();
			Focus();
				
			float opacity = (float)Opacity;
			var aTimer = new Timer();
			aTimer.Interval = 10;
			aTimer.Tick += (sender, execute) => {
				opacity += 0.05f;

				Opacity = Math.Min(opacity, 0.999);
				
				if (opacity >= 1f)
					aTimer.Dispose();
			};
		    aTimer.Enabled = true;
		}
		
		public void FadeOut(Action callback = null) {
       		float opacity = (float)Opacity;
			var aTimer = new Timer();
			aTimer.Interval = 10;
			aTimer.Tick += (sender, execute) => {
				Opacity = opacity -= 0.05f;
				
				if (opacity <= 0f) {
					aTimer.Dispose();
					
					Hide();

					callback?.Invoke();
				}
			};
		    aTimer.Enabled = true;
		}
		
		public void RegisterHotkey(HotkeyHandler hotkey) {
			if (hotkey.IsRegistered)
				return;
			
			Launchers.Hotkey = hotkey;
			if (!Launchers.Hotkey.Register(this)) {
				MessageBox.Show($"Failed to register hotkey: {Launchers.Hotkey}", "Fatal error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				FadeOut(Close);
			}
		}
		
		public void UnregisterHotkey() {
			if (Launchers?.Hotkey.IsRegistered == true)
				Launchers.Hotkey.Unregister();
		}

		public void RefreshBackground() {
			string bgPath = Background.GetCurrentDesktopWallpaper();
			if (bgPath == lastBackgroundImagePath) {
				if (bgPath != null)
					return;

				var newBgColor = Color.FromArgb(SystemColors.Desktop.ToArgb());
				if (newBgColor == lastBackgroundColor)
					return;
			}

			lastBackgroundImagePath = bgPath;

			if (lastBackgroundImagePath == null) {
				lastBackgroundColor = Color.FromArgb(SystemColors.Desktop.ToArgb());

				BackgroundImage = null;
				BackColor = lastBackgroundColor;
			} else {
				var bmp = (Bitmap)Bitmap.FromFile(lastBackgroundImagePath);

				lastBackgroundColor = ImageProcessor.GetAverageColor(bmp);

				BackgroundImage = bmp;
				BackColor = lastBackgroundColor;
			}

			foreach (var control in Controls.OfType<ButtonLauncher>())
				control.RefreshColor();
		}

		protected override void OnLoad(EventArgs e) {
			RegisterHotkey(Launchers.Hotkey);
		}
		
		protected override void OnPaintBackground(PaintEventArgs e) {
			e.Graphics.FillRectangle(new SolidBrush(BackColor), 0, 0, Width, Height);
			
			int taskBarHeight = Height - Screen.FromControl(this).WorkingArea.Height;
			
			if(BackgroundImage == null)
				return;
			
			float imgRatio = BackgroundImage.Height/(float)BackgroundImage.Width;
			float containerRatio = Height/(float)Width;
			
			int width, height;
			
			if (containerRatio > imgRatio) {
				height = Height;
				width = (int)Math.Ceiling(Height / imgRatio);
			} else {
				width = Width;
				height = (int)Math.Ceiling(Width * imgRatio);
			}
			
			int posY = -Math.Abs(height - Height - taskBarHeight)/2;
			
			if (posY + height < Height)
				posY = Height - height;
			
			e.Graphics.DrawImage(BackgroundImage, -Math.Abs(width - Width)/2, posY, width, height);
		}
		
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
			if (keyData == (Keys.Alt | Keys.F4)) {
				FadeOut(Close);
				return true;
			} else if (keyData == Keys.Escape) {
				FadeOut();
				return true;
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}
		
		protected override void WndProc(ref Message m) {
            if (m.Msg == HotkeyHandler.WM_HOTKEY_MSG_ID)
				FadeIn();
            
            base.WndProc(ref m);
        }
		
		protected override void OnFormClosed(FormClosedEventArgs e) {
			base.OnFormClosed(e);
			
			instances--;
			
			if (instances <= 0)
				Application.Exit();
		}
	}
}
