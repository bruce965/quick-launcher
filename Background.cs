﻿using System;
using System.Runtime.InteropServices;

namespace QuickLauncher
{
	class Background
	{
		private const UInt32 SPI_GETDESKWALLPAPER = 0x73;
		private const int MAX_PATH = 260;

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int SystemParametersInfo(UInt32 uAction, int uParam, string lpvParam, int fuWinIni);

		public static string GetCurrentDesktopWallpaper() {
			string currentWallpaper = new String('\0', MAX_PATH);
			SystemParametersInfo(SPI_GETDESKWALLPAPER, currentWallpaper.Length, currentWallpaper, 0);
			currentWallpaper = currentWallpaper.Substring(0, currentWallpaper.IndexOf('\0'));

			return String.IsNullOrEmpty(currentWallpaper) ? null : currentWallpaper;
		}
	}
}

