﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace QuickLauncher
{
	public static class ImageProcessor
	{
		public static Color GetAverageColor(Bitmap bitmap) {
			return bitmap.getPixels().average();
		}
		
		static IEnumerable<Color> getPixels(this Bitmap bitmap) {
			// TODO: optimize and read all pixels.
			for (var y = 0; y < bitmap.Height; y += 50)
				for (var x = 0; x < bitmap.Width; x += 50)
					yield return bitmap.GetPixel(x, y);
		}
		
		static Color average(this IEnumerable<Color> colors) {
			double r = 0;
			double g = 0;
			double b = 0;
			//double a = 0;
			long count = 0;
			
			foreach (var color in colors) {
				r += color.R;
				g += color.G;
				b += color.B;
				//a += color.A;
				count++;
			}
			
			return Color.FromArgb(
				255 << 24 |  //(byte)Math.Round(a / count) |
				(byte)Math.Round(r / count) << 16 |
				(byte)Math.Round(g / count) << 8 |
				(byte)Math.Round(b / count)
			);
		}
	}
}
